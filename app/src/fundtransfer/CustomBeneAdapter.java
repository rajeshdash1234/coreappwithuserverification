package com.iserveu.aeps.fundtransfer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iserveu.aeps.R;

import java.util.ArrayList;

public class CustomBeneAdapter extends BaseAdapter {
    Context context;

    ArrayList<String> beneName;
    ArrayList<String> beneAccount;
    ArrayList<String> beneBank;
    LayoutInflater inflter;

    public CustomBeneAdapter(Context applicationContext, ArrayList<String> beneName, ArrayList<String> beneAccount, ArrayList<String> beneBank) {
        this.context = applicationContext;
        this.beneName = beneName;
        this.beneAccount = beneAccount;
        this.beneBank = beneBank;
        inflter = (LayoutInflater.from(applicationContext));
    }


    @Override
    public int getCount() {
        return beneAccount.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.listview_item, null);
//        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        TextView names = (TextView) view.findViewById(R.id.textView);
        TextView names1 = (TextView) view.findViewById(R.id.textView1);
        TextView names2 = (TextView) view.findViewById(R.id.textView2);
//        icon.setImageResource(flags[i]);
        names.setText(beneName.get(i));
        names1.setText(beneAccount.get(i));
        names2.setText(beneBank.get(i));
        return view;
    }
}
