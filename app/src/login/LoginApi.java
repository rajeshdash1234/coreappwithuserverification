package com.iserveu.aeps.login;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface LoginApi {
    @POST()

    Call<LoginResponse> insertUser(@Header("Authorization") String token, @Url String url);

    //"/aeps/userlogin"
}
