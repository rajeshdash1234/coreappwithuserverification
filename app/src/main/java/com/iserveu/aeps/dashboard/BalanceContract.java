package com.iserveu.aeps.dashboard;

import java.util.ArrayList;

public class BalanceContract {
    /**
     * View interface sends report list to ReportActivity
     */
    public interface View {

        /**
         * showReports() showReports on ReportActivity
         */
        void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures);
        void showBalance(String balance);
        void emptyLogin();
        void showLoader();
        void hideLoader();

    }

    /**
     * UserActionsListener interface checks the load of Reports
     */
    interface
    UserActionsListener {
        void loadBalance(String token);
    }

}
