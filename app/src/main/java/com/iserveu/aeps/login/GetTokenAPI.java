package com.iserveu.aeps.login;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface GetTokenAPI {
    @POST()
   // @FormUrlEncoded
    @Headers("Content-Type: application/json")
    Call<GetTokenResponse> getToken(@Body GetTokenRequest body, @Url String url);

}

