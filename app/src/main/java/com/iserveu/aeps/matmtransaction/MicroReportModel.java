package com.iserveu.aeps.matmtransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MicroReportModel {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("previousAmount")
    @Expose
    private String previousAmount;

    @SerializedName("amountTransacted")
    @Expose
    private String amountTransacted;

    @SerializedName("balanceAmount")
    @Expose
    private String balanceAmount;

    @SerializedName("amountOutstanding")
    @Expose
    private String amountOutstanding;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("transactionType")
    @Expose
    private String transactionType;

    @SerializedName("userName")
    @Expose
    private String userName;


    @SerializedName("userTrackId")
    @Expose
    private String userTrackId;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    @SerializedName("apiComment")
    @Expose
    private String apiComment;

    @SerializedName("cardNumber")
    @Expose
    private String cardNumber;

    @SerializedName("rrn")
    @Expose
    private String rrn;

    @SerializedName("operationPerformed")
    @Expose
    private String operationPerformed;

    @SerializedName("apiDate")
    @Expose
    private String apiDate;



    public MicroReportModel(){

    }

    public String getApiComment() {
        return apiComment;
    }

    public void setApiComment(String apiComment) {
        this.apiComment = apiComment;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getOperationPerformed() {
        return operationPerformed;
    }

    public void setOperationPerformed(String operationPerformed) {
        this.operationPerformed = operationPerformed;
    }

    public String getApiDate() {
        return apiDate;
    }

    public void setApiDate(String apiDate) {
        this.apiDate = apiDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        this.previousAmount = previousAmount;
    }

    public String getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(String amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getAmountOutstanding() {
        return amountOutstanding;
    }

    public void setAmountOutstanding(String amountOutstanding) {
        this.amountOutstanding = amountOutstanding;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserTrackId() {
        return userTrackId;
    }

    public void setUserTrackId(String userTrackId) {
        this.userTrackId = userTrackId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
