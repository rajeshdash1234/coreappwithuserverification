package com.iserveu.aeps.onboarding;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class OnboardingViewPagerAdapter extends FragmentPagerAdapter {

        private int COUNT = 6;

    OnboardingViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new UsernameVerificationFragment();
                    break;
                case 1:
                    fragment = new MpinVerificationFragment();
                    break;
                case 2:
                    fragment = new PasswordVerificationFragment();
                    break;
                case 3:
                    fragment = new VideoVerificationFragment();
                    break;
                case 4:
                    fragment = new OtpVerificationFragment();
                    break;
                case 5:
                    fragment = new EmailVerificationFragment();
                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return COUNT;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return "Tab " + (position + 1);
        }
    }

