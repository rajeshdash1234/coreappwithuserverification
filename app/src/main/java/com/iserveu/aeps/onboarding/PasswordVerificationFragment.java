package com.iserveu.aeps.onboarding;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Util;

import cdflynn.android.library.checkview.CheckView;

public class PasswordVerificationFragment extends Fragment {


    private Button validate_pwd;
    private EditText edit_password,edit_reenter_password;
    OnboardingActivity activity;
    CheckView mCheckView;
    ImageView log_img;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_password_verification, container, false);
        mCheckView = (CheckView)rootView.findViewById(R.id.check);
        edit_password = (EditText)rootView.findViewById(R.id.edit_password);
        edit_reenter_password = (EditText)rootView.findViewById(R.id.edit_reenter_password);
        validate_pwd =(Button)rootView.findViewById(R.id.validate_pwd);
        log_img = rootView.findViewById(R.id.log_img);
        activity = (OnboardingActivity) getActivity();
        validate_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit_password.getText().toString().isEmpty()){
                    edit_password.setError("Please enter your password.");
                }else if(!Util.isValidPassword(edit_password.getText().toString())){
                    edit_password.setError("Please enter your correct password format.");
                }
                else if(!edit_reenter_password.getText().toString().equalsIgnoreCase(edit_password.getText().toString())){
                    edit_reenter_password.setError("Your password and re-entered password doesnot match.");
                }else{
                    //mCheckView.check();
                   // activity.passwordVerifiedUpdate(true);
                    Constants.VERIFIED_USER_PWD = edit_password.getText().toString();
                    log_img.setVisibility(View.GONE);
                    mCheckView.check();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            activity.passwordVerifiedUpdate(true);
                        }
                    }, 500);
                    mCheckView.check();
                }
            }
        });
        return rootView;
    }
}
