package com.iserveu.aeps.rechargereport;

public class RechargeResponseSetGet {

    @Override
    public String toString() {
        return "RechargeResponseSetGet{" +
                "operatorDescription='" + operatorDescription + '\'' +
                ", apiName='" + apiName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", amountTransacted='" + amountTransacted + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", userName='" + userName + '\'' +
                ", balanceAmount='" + balanceAmount + '\'' +
                ", previousAmount='" + previousAmount + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", userTrackId='" + userTrackId + '\'' +
                ", id='" + id + '\'' +
                ", apiComment='" + apiComment + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    private String operatorDescription;

    private String apiName;

    private String mobileNumber;

    private String amountTransacted;

    private String updatedDate;

    private String userName;

    private String balanceAmount;

    private String previousAmount;

    private String transactionType;

    private String createdDate;

    private String operatorTransactionId;

    private String userTrackId;

    private String id;

    private String apiComment;

    private String status;

    public String getOperatorDescription ()
    {
        return operatorDescription;
    }

    public void setOperatorDescription (String operatorDescription)
    {
        this.operatorDescription = operatorDescription;
    }

    public String getApiName ()
    {
        return apiName;
    }

    public void setApiName (String apiName)
    {
        this.apiName = apiName;
    }

    public String getMobileNumber ()
    {
        return mobileNumber;
    }

    public void setMobileNumber (String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getAmountTransacted ()
    {
        return amountTransacted;
    }

    public void setAmountTransacted (String amountTransacted)
    {
        this.amountTransacted = amountTransacted;
    }

    public String getUpdatedDate ()
    {
        return updatedDate;
    }

    public void setUpdatedDate (String updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public String getUserName ()
    {
        return userName;
    }

    public void setUserName (String userName)
    {
        this.userName = userName;
    }

    public String getBalanceAmount ()
    {
        return balanceAmount;
    }

    public void setBalanceAmount (String balanceAmount)
    {
        this.balanceAmount = balanceAmount;
    }

    public String getPreviousAmount ()
    {
        return previousAmount;
    }

    public void setPreviousAmount (String previousAmount)
    {
        this.previousAmount = previousAmount;
    }

    public String getTransactionType ()
    {
        return transactionType;
    }

    public void setTransactionType (String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getCreatedDate ()
    {
        return createdDate;
    }

    public void setCreatedDate (String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getOperatorTransactionId ()
    {
        return operatorTransactionId;
    }

    public void setOperatorTransactionId (String operatorTransactionId)
    {
        this.operatorTransactionId = operatorTransactionId;
    }

//    public String getUserTrackId ()
//    {
//        return userTrackId;
//    }
//
//    public void setUserTrackId (String userTrackId)
//    {
//        this.userTrackId = userTrackId;
//    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getApiComment ()
    {
        return apiComment;
    }

    public void setApiComment (String apiComment)
    {
        this.apiComment = apiComment;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    /*@Override
    public String toString()
    {
        return "ClassPojo [operatorDescription = "+operatorDescription+", apiName = "+apiName+", mobileNumber = "+mobileNumber+", amountTransacted = "+amountTransacted+", updatedDate = "+updatedDate+", userName = "+userName+", balanceAmount = "+balanceAmount+", previousAmount = "+previousAmount+", transactionType = "+transactionType+", createdDate = "+createdDate+", operatorTransactionId = "+operatorTransactionId+", userTrackId = "+userTrackId+", id = "+id+", apiComment = "+apiComment+", status = "+status+"]";
    }*/

}
