package com.iserveu.aeps.rechargereport;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ReportRechargeApi {
    @POST()
//    @POST("/transactiondetails")
    Call<ReportRechargeResponse> getReportRecharge(@Header("Authorization") String token, @Body ReportRechargeRequest reportRechargeRequestBody, @Url String url);
}
