package com.iserveu.aeps.refund;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface RefundAPI {
    //"/aeps/transenquiry"
    @POST()
    Call<RefundResponse> checkRefund(@Header("Authorization") String token, @Body RefundRequestModel body, @Url String url);
}

