package com.iserveu.aeps.settings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.finopaytech.finosdk.activity.DeviceSettingActivity;
import com.iserveu.aeps.R;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.utils.Session;
import com.rw.loadingdialog.LoadingView;

import java.util.HashMap;

import fr.ganfra.materialspinner.MaterialSpinner;
import static com.iserveu.aeps.utils.Util.showProgress;

public class SettingsActivity extends AppCompatActivity {

    private MaterialSpinner deviceSpinner;
    private static final String[] ITEMS = {"Morpho","Mantra"};
    UsbManager musbManager;
    private UsbDevice usbDevice;
    LoadingView loadingView;
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String morphodeviceid="SAGEM SA";
    String mantradeviceid="MANTRA";
    String morphoe2device="Morpho";
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setToolbar();
        session = new Session(this);
        deviceSpinner = findViewById(R.id.deviceSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deviceSpinner.setAdapter(adapter);

        deviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    showLoader();
                    musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                    updateDeviceList ();
                }
                if(position == 1){
                    showLoader();
                    musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
                    updateDeviceList ();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
//        Toast.makeText(this, "on resume", Toast.LENGTH_SHORT).show();
        if(deviceSpinner.getSelectedItemPosition() == 2){
            devicecheck();
        }else if(deviceSpinner.getSelectedItemPosition() == 1){
            devicecheck();
        }
    }

    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        hideLoader ();
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            // Toast.makeText(DashboardActivity.this, "No Devices Currently Connected" + usbconnted, Toast.LENGTH_LONG).show();
            deviceConnectMessgae ();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(device !=null && device.getManufacturerName () != null){
                        if(device.getManufacturerName ().equalsIgnoreCase ( mantradeviceid )||device.getManufacturerName ().equalsIgnoreCase ( morphodeviceid )||device.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )){
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName ();
                            session.setUsbDevice(usbDevice.getManufacturerName ());
                        }
                    }
                }
            }
            devicecheck ();
        }
    }



    public void showLoader() {
        if (loadingView ==null){
            loadingView = showProgress(this);
        }
        loadingView.show();
    }

    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }
    }

    private void deviceConnectMessgae (){
        hideLoader();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect))
                .setMessage(getResources().getString(R.string.decive_please_connect))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();
    }
    private void rdserviceMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_install))
                .setMessage(getResources().getString(R.string.mantra_rd_service))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private void mantraMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    private void morphoMessage(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.morpho))
                .setMessage(getResources().getString(R.string.install_morpho_message))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         * play store intent
                         */
                        final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity (new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void devicecheck() {
        if(usbDevice == null) {
            deviceConnectMessgae ();
        }else {
            if(deviceSpinner.getSelectedItemPosition() == 1){
                if (usbDevice.getManufacturerName ().trim ().equalsIgnoreCase ( morphodeviceid )||usbDevice.getManufacturerName().trim ().equalsIgnoreCase ( morphoe2device )) {
                    morphoinstallcheck ();
                }else{
                    deviceConnectMessgae();
                }
            }else if(deviceSpinner.getSelectedItemPosition() == 2) {
                if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(mantradeviceid)) {
                    installcheck();
                } else {
                    deviceConnectMessgae();
                }
            }
        }
    }

    private  void installcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");
        if(isAppInstalled) {
// This intent will help you to launch if the package is already installed
            if (serviceAppInstalled){
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.INFO");
                intent.setPackage ( "com.mantra.rdservice" );
                startActivityForResult ( intent, 1 );
            }else{
                rdserviceMessage ();

            }
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage ();
        }
    }
    private  void morphoinstallcheck(){
        boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");
        if(isAppInstalled) {
//This intent will help you to launch if the package is already installed
            Intent intent1 = new Intent ();
            intent1.setAction ( "in.gov.uidai.rdservice.fp.INFO" );
            intent1.setPackage ( "com.scl.rdservice" );
//            intent1.addFlags ( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivityForResult ( intent1, 2 );
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            morphoMessage ();
        }
    }

    /*
   app installation check
   */
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                hideLoader();
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText ( this, "Your device is ready for use", Toast.LENGTH_SHORT ).show ();
                                Intent intent = new Intent(SettingsActivity.this,DashboardActivity.class);
                                startActivity(intent);
                                finish ();
                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "result"+""+result, Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.hide();
                        }
                    }
                }
                break;
            case 2:
                if (resultCode == RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                hideLoader();
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText ( this, "Your device is ready for use", Toast.LENGTH_SHORT ).show ();
                                Intent intent = new Intent(SettingsActivity.this,DashboardActivity.class);
                                startActivity(intent);
                                finish ();
                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "Decive info check", Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.hide();
                        }
                    }
                }
                break;
        }
    }
    private void setToolbar() {
        Toolbar mToolbar = findViewById ( R.id.toolbar );
        mToolbar.setTitle (getResources().getString(R.string.settings) );
        mToolbar.inflateMenu ( R.menu.bank_menu );
        mToolbar.setOnMenuItemClickListener ( new Toolbar.OnMenuItemClickListener () {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()==R.id.action_close) {
                    Intent intent = new Intent(SettingsActivity.this,DashboardActivity.class);
                    startActivity(intent);
                    finish ();
                }
                return false;
            }
        } );
    }
}