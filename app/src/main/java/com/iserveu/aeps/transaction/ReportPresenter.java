package com.iserveu.aeps.transaction;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.iserveu.aeps.utils.AEPSAPIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.BuildConfig.GET_AEPS_REPORT_URL;


/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class ReportPresenter implements ReportContract.UserActionsListener {
    /**
     * Initialize ReportContractView
     */
    private ReportContract.View reportView;
    private AEPSAPIService aepsapiService;
    private ArrayList<ReportResponse> reportResponseArrayList ;
    /**
     * Initialize ReportPresenter
     */
    public ReportPresenter(ReportContract.View reportView) {
        this.reportView = reportView;
    }

    @Override
    public void loadReports(final String fromDate,final String toDate,final String token) {

        if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("") ) {
            reportView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }

            AndroidNetworking.get(GET_AEPS_REPORT_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            encryptedReport(fromDate,toDate,token,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


        } else {
            reportView.emptyDates();

            //loginView.checkEmptyFields();
        }
    }



    /**
     *  load Reports of  ReportActivity
     * @param fromDate
     * @param toDate
     * @param token
     * @param encodedUrl
     */
   public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl){
       final ReportAPI reportAPI =
           this.aepsapiService.getClient().create(ReportAPI.class);

       Call<ReportResponse> call = reportAPI.insertUser(token,new ReportRequest(fromDate,toDate),encodedUrl);


       call.enqueue(new Callback<ReportResponse>() {
           @Override
           public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {
                /*response.body(); // have your all data
                String userName = response.body().getStatus();*/
               if(response.isSuccessful()) {

                   ReportResponse reportResponse = response.body();
                   Log.v("Laxmi","hfh"+reportResponse);

                   if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                       ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                       double totalAmount = 0;
                       for(int i = 0; i<result.size(); i++) {
                           totalAmount += Double.parseDouble(result.get(i).getAmount());
                       }
                       reportView.reportsReady(result, String.valueOf(totalAmount));
                   }
               }
               reportView.hideLoader();
               reportView.showReports();
           }

           @Override
           public void onFailure(Call<ReportResponse> call, Throwable t) {

               reportView.hideLoader();
               reportView.showReports();

           }
       });
   }

}
