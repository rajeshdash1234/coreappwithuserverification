package com.iserveu.aeps.utils;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

public class PermissionManager {

    private final AppCompatActivity activity;
    private int requestCode;
    private String permission;
    private OnRequestPermissionListener permissionListener;

    /**
     * Gets instance.
     *
     * @param activity the activity
     * @return the instance
     */
    public static PermissionManager getInstance(AppCompatActivity activity) {
        synchronized (PermissionManager.class) {
            return new PermissionManager(activity);
        }
    }

    /**
     * Instantiates a new Permission manager.
     *
     * @param activity the activity
     */
    public PermissionManager(AppCompatActivity activity) {
        this.activity = activity;
    }

    /**
     * Load permission.
     *
     * @param permission  the permission
     * @param requestCode the request code
     */
    public void loadPermission(String permission, int requestCode) {
        this.permission = permission;
        this.requestCode = requestCode;
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
        } else {
            if (permissionListener != null)
                permissionListener.onRequestGranted(requestCode, permission);
        }
    }

    /**
     * On request permissions result.
     *
     * @param requestCode  the request code
     * @param permissions  the permissions
     * @param grantResults the grant results
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == this.requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissionListener != null)
                    permissionListener.onRequestGranted(requestCode, permission);
            } else {
                if (permissionListener != null)
                    permissionListener.onRequestDeclined(requestCode, permission);
            }
        }
    }

    /**
     * Sets on request permission listener.
     *
     * @param permissionListener the permission listener
     */
    public void setOnRequestPermissionListener(OnRequestPermissionListener permissionListener) {
        this.permissionListener = permissionListener;
    }

    /**
     * The interface On request permission listener.
     */
    public interface OnRequestPermissionListener {
        /**
         * On request granted.
         *
         * @param requestCode the request code
         * @param permission  the permission
         */
        void onRequestGranted(int requestCode, String permission);

        /**
         * On request declined.
         *
         * @param requestCode the request code
         * @param permission  the permission
         */
        void onRequestDeclined(int requestCode, String permission);
    }
}
