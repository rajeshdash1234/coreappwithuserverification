package com.iserveu.aeps.main2activity;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iserveu.aeps.R;
import com.iserveu.aeps.dashboard.BalanceContract;
import com.iserveu.aeps.dashboard.BalancePresenter;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.dashboard.UserInfoModel;
import com.iserveu.aeps.fundtransfer.FundTransfer;
import com.iserveu.aeps.fundtransferreport.FundTransferReport;
import com.iserveu.aeps.login.LoginContract;
import com.iserveu.aeps.login.LoginPresenter;
import com.iserveu.aeps.microatm.MicroAtmActivity;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Main2Activity extends AppCompatActivity implements BalanceContract.View, LoginContract.View{

    RelativeLayout fundtransfer_relative_layout,aeps_relative_layout,fundtransfer_report_relative_layout;
    TextView balanceMoney,customer_name;
    private LoginPresenter loginPresenter;
    private BalancePresenter mActionsListener;
    Session session;
    private ImageView refresh_Payment,action_logout;
    private CircleImageView customer_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        loginPresenter = new LoginPresenter(Main2Activity.this);
        session = new Session(Main2Activity.this);
        mActionsListener = new BalancePresenter(Main2Activity.this);
        balanceMoney = findViewById ( R.id. balanceMoney );

        fundtransfer_relative_layout = findViewById(R.id.fundtransfer_relative_layout);
        fundtransfer_report_relative_layout = findViewById(R.id.fundtransfer_report_relative_layout);
        aeps_relative_layout = findViewById(R.id.aeps_relative_layout);

        fundtransfer_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransfer.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
        refresh_Payment = (ImageView)findViewById(R.id.refresh_Payment);
        refresh_Payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rorate_Clockwise(refresh_Payment);
                mActionsListener.loadBalance(session.getUserToken());
                System.out.println(">>>>>------Rajesh");
            }
        });

        customer_name = (TextView)findViewById(R.id.customer_name);
        customer_name.setText("Hello, "+Constants.USER_NAME);

        customer_image = (CircleImageView)findViewById(R.id.customer_image);
        loadProfileImage();

        action_logout = (ImageView) findViewById(R.id.action_logout);
        action_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // onBackPressed();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(Main2Activity.this);
                builder1.setMessage("Do you want to logout this app.");
                builder1.setTitle("Log out");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                onBackPressed();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        aeps_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }

            }
        });

        fundtransfer_report_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransferReport.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadProfileImage(){
        String image_url ="https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/ADMIN_PROFILE%2F"+Constants.ADMIN_NAME+"%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";
        Glide.with(Main2Activity.this)
                .load(image_url)
                .apply(RequestOptions.placeholderOf(R.drawable.user_icon).error(R.drawable.user_icon))
                .into(customer_image);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActionsListener.loadBalance(session.getUserToken());
    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {
        String aeps = "27";
        String dmt = "30";
        ArrayList<String> featureCode = new ArrayList<>();
        featureCode.add(aeps);
        featureCode.add(dmt);
        for (int i = 0; i < userFeatures.size(); i++) {
            if(featureCode.contains(userFeatures.get(i).getId())) {
                if(aeps.equalsIgnoreCase(userFeatures.get(i).getId())){
                    aeps_relative_layout.setVisibility(View.VISIBLE);
//                    aepsTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aepsFeatureCode);
                    loginPresenter.performLogin(session.getUserName(),session.getPassword());
                }
                if(dmt.equalsIgnoreCase(userFeatures.get(i).getId())){
                    fundtransfer_relative_layout.setVisibility(View.VISIBLE);
                    fundtransfer_report_relative_layout.setVisibility(View.VISIBLE);
//                    matmTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(matmFeatureCode);
                }
//                if(aeps2FeatureCode.equalsIgnoreCase(userFeatures.get(i).getId())){
//                    aeps2Option.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aeps2FeatureCode);
//                }
            } else {
                Log.v("radha", "not contains : "+userFeatures.get(i).getId());
            }
        }

    }

    @Override
    public void showBalance(String balance) {
        balanceMoney.setText(balance);

    }

    @Override
    public void emptyLogin() {

    }

    @Override
    public void checkLoginStatus(String status, String message, String token, String nextFreshnessFactor) {

    }

    @Override
    public void showLoginFeature(ArrayList<UserInfoModel.userFeature> userFeatures, String token) {
        loginPresenter.getLoginDetails(token);
    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }
    public void rorate_Clockwise(View view) {
        ObjectAnimator rotate = ObjectAnimator.ofFloat(view, "rotation", 0f, 180f);
        rotate.setRepeatCount(5);
        rotate.setDuration(500);
        rotate.start();
    }
}