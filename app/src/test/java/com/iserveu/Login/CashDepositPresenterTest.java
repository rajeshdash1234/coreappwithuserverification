/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iserveu.Login;


import com.iserveu.aeps.deposit.CashDepositContract;
import com.iserveu.aeps.deposit.CashDepositPresenter;
import com.iserveu.aeps.deposit.CashDepositRequestModel;
import com.iserveu.aeps.utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link CashDepositPresenterTest}.
 */
public class CashDepositPresenterTest {


    /**
     * Mock of CashDepositContract.View for unit testing
     */
    @Mock
    private CashDepositContract.View cashDepositContractView;


    /**
     * Mock of CashDepositPresenter for unit testing
     */
    @InjectMocks
    private CashDepositPresenter cashDepositPresenter;


    /**
     * setupCashDepositPresenterTest will be called before execution of the tests
     */

    @Before
    public void setupCashDepositPresenterTest() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test

        cashDepositPresenter = new CashDepositPresenter(cashDepositContractView);


    }


    /**
     * emptyTokenErrorUi() checks whether token is empty string  or not
     */

    @Test
    public void emptyTokenErrorUi() {
        // When the presenter is asked to check cash deposit with null request and empty token
        cashDepositPresenter.performCashDeposit("", null);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * nullTokenErrorUi() checks whether token is null or not
     */

    @Test
    public void nullTokenErrorUi() {
        // When the presenter is asked to check cash deposit with null request and token
        cashDepositPresenter.performCashDeposit(null, null);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * nullRequest() checks whether request model is null or not
     */

    @Test
    public void nullRequest() {
        // When the presenter is asked to check cash deposit with null request
        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, null);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * emptyAmountRequest() checks whether amount is empty string or not
     */

    @Test
    public void emptyAmountRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like amount
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * nullAmountRequest() checks whether amount is null or not
     */

    @Test
    public void nullAmountRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like amount
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel(null, "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * emptyAadharNoRequest() checks whether aadhar no is empty string or not
     */

    @Test
    public void emptyAadharNoRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like aadhar no
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * nullAadharNoRequest() checks whether aadhar no is null or not
     */

    @Test
    public void nullAadharNoRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like aadhar no
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", null, "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * emptyMobileNoRequest() checks whether mobilenumber is empty or not
     */

    @Test
    public void emptyMobileNoRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like mobile
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * nullMobileNoRequest() checks whether mobilenumber is null or not
     */

    @Test
    public void nullMobileNoRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like mobile
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", null, "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * nullFreshnessFactorRequest() checks whether freshness factor is null or not
     */

    @Test
    public void nullFreshnessFactorRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like freshnessfactor
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", null, "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * emptyFreshnessFactorRequest() checks whether freshness factor is empty or not
     */

    @Test
    public void emptyFreshnessFactorRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like freshness factor
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * emptyOperationRequest() checks whether operation is empty or not
     */


    @Test
    public void emptyOperationRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like operation
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * nullOperationRequest() checks whether operation  is null or not
     */

    @Test
    public void nullOperationRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like operation
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", null, "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * emptyCiRequest() checks whether ci is empty or not
     */
    @Test
    public void emptyCiRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like ci
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }


    /**
     * nullCiRequest() checks whether ci  is null or not
     */

    @Test
    public void nullCiRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like ci
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", null, "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyDcRequest() checks whether dc is empty or not
     */
    @Test
    public void emptyDcRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like dc
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullDcRequest() checks whether dc  is null or not
     */

    @Test
    public void nullDcRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like dc
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", null, "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyDpidRequest() checks whether dpid is empty or not
     */
    @Test
    public void emptyDpidRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like dpid
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullDpidRequest() checks whether dpid  is null or not
     */

    @Test
    public void nullDpidRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like dpid
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", null, "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyEncryptedPidRequest() checks whether encryptedPID is empty or not
     */
    @Test
    public void emptyEncryptedPidRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like encryptedPID
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullEncryptedPidRequest() checks whether encryptedPID  is null or not
     */

    @Test
    public void nullEncryptedPidRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like encryptedPID
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", null, "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyHMacRequest() checks whether hMac is empty or not
     */
    @Test
    public void emptyHMacRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like hMac
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullHMacRequest() checks whether hMac  is null or not
     */

    @Test
    public void nullHMacRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like hMac
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", null, "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyBankIinRequest() checks whether Bank Iin Number is empty or not
     */
    @Test
    public void emptyBankIinRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like Bank Iin Number
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullBankIinRequest() checks whether Bank Iin Number  is null or not
     */

    @Test
    public void nullBankIinRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like Bank Iin Number
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", null, "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptymcDataRequest() checks whether mcData is empty or not
     */
    @Test
    public void emptymcDataRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like mcData
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullmcDataRequest() checks whether mcData  is null or not
     */

    @Test
    public void nullmcDataRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like mcData
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", null, "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyMiRequest() checks whether mi is empty or not
     */
    @Test
    public void emptyMiRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like mi
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullMiRequest() checks whether mi  is null or not
     */

    @Test
    public void nullMiRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like mi
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", null, "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyRdsIdRequest() checks whether Rdsid is empty or not
     */
    @Test
    public void emptyRdsIdRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like Rdsid
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullRdsIdRequest() checks whether Rdsid  is null or not
     */

    @Test
    public void nullRdsIdRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like Rdsid
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", null, "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyRdsverRequest() checks whether Rdsver is empty or not
     */
    @Test
    public void emptyRdsverRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like Rdsver
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullRdsverRequest() checks whether Rdsver  is null or not
     */

    @Test
    public void nullRdsverRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like Rdsver
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", null, "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyskeyRequest() checks whether Skey is empty or not
     */
    @Test
    public void emptyskeyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like Skey
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }
    /**
     * nullSkeyRequest() checks whether Skey  is null or not
     */

    @Test
    public void nullSkeyRequest() {
        // When the presenter is asked to check cash deposit with null field(s) like Skey
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", null);

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }

    /**
     * emptyAmountAndAadharEmptyRequest() checks whether amount and aadhar both are  empty or not
     */

    @Test
    public void emptyAmountAndAadharEmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like amount , aadhar no
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("", "", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * emptyAmountAndMobileEmptyRequest() checks whether amount and mobile no both are  empty or not
     */

    @Test
    public void emptyAmountAndMobileEmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like amount , mobile number
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * emptyAmountAndFreshnessFactorEmptyRequest() checks whether amount and freshness factor no both are  empty or not
     */

    @Test
    public void emptyAmountAndFreshnessFactorEmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like amount , freshness factor
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }

    /**
     * emptyAadharAndFreshnessFactorEmptyRequest() checks whether aadhar and freshness factor no both are  empty or not
     */

    @Test
    public void emptyAadharAndFreshnessFactorEmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like aadhar , freshness factor
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();

    }



    /**
     * emptyAmountAndOperationEmptyRequest() checks whether amount and operation both are  empty or not
     */

    @Test
    public void emptyAmountAndOpeartionEmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like amount , operation
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * emptyAadharAndMobileEmptyRequest() checks whether aadhar and mobile no both are  empty or not
     */

    @Test
    public void emptyAadharAndMobileEmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like aadhar no , mobile number
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * emptyAadharAndOperationEmptyRequest() checks whether aadhar and operation both are  empty or not
     */

    @Test
    public void emptyAadharAndOperationEmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like aadhar no , operation
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();


    }


    /**
     * EmptyRequest() checks whether  amount , aadhar no , operation , mobile number are  empty or not
     */


    @Test
    public void EmptyRequest() {
        // When the presenter is asked to check cash deposit with empty field(s) like amount , aadhar no , operation , mobile number
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("", "", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "", "", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashDepositContractView).checkEmptyFields();
    }


    /**
     * successRequest() checks data is successfully posted
     */


    @Test
    public void successRequest() {
        // When the presenter is asked to load cash deposit with dummy success data
        CashDepositRequestModel cashDepositRequestModel = new CashDepositRequestModel("1", "633735238387", "20191230", "e0b84d64-5a1a-444d-b935-7265e5d7da27", "0484332", "MANTRA.MSIPL", "MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh", "-4383403874852159559", "2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF", "607152", "MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK", "MFS100", "7377688748", "DEPOSIT", "MANTRA.WIN.001", "1.0.0", "rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashDepositPresenter.performCashDeposit(Constants.TEST_USER_TOKEN, cashDepositRequestModel);
        // When show Success message in  UI

        cashDepositContractView.checkCashDepositStatus("0", "Successful");
        // Then show Success message in Test UI
        verify(cashDepositContractView).checkCashDepositStatus("0", "Successful");


    }
}
