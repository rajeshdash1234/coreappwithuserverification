/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iserveu.Login;


import com.iserveu.aeps.login.LoginContract;
import com.iserveu.aeps.login.LoginPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link LoginPresenterTest}.
 */
public class LoginPresenterTest {



    /**
     *  Mock of loginView for unit testing
     */
    @Mock
    private LoginContract.View loginView;




    @InjectMocks
    private LoginPresenter loginPresenter;



    /**
     *  setupLoginPresenterTest will be called before execution of the tests
     */

    @Before
    public void setupLoginPresenterTest() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test

        loginPresenter = new LoginPresenter(loginView);


    }


    /**
     *  loginEmptyFieldsShowsErrorUi() checks whether username and password fields are empty
     */
    @Test
    public void loginEmptyFieldsShowsErrorUi() {
        // When the presenter is asked to login with empty field(s)
        loginPresenter.performLogin("", "");

        // Then an empty field error is shown in the UI
        verify(loginView).checkEmptyFields();



    }

    /**
     *  loginFailedUi() checks the invalid user is trying to login and throws a failed error
     */
    @Test
    public void loginFailedUi() {

        // When the presenter is asked to login with invalid username and password
        loginPresenter.performLogin("ggg", "df");

        //When
        loginView.checkLoginStatus("","Login Failed","","");

        // Then a failed error is shown in the UI
        verify(loginView).checkLoginStatus("","Login Failed","","");
    }


    /**
     *  loginSuccessUi() checks the successful login of the user
     */

    @Test
    public void loginSuccessUi() {
        // When the presenter is asked to save an empty note

        loginPresenter.performLogin("itpl", "namodi");

        //When
        loginView.checkLoginStatus("0","Login Successful","","");

        // Then an empty not error is shown in the UI
       verify(loginView).checkLoginStatus("0","Login Successful","","");
    }



}
